#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <dirent.h>

#define MAXENV 1024
#ifdef PATH_MAX
     #define MAXPATH PATH_MAX
#else
     #define MAXPATH 8192
#endif

static char nm[MAXENV];
static char wd[MAXPATH+1];

int main(int argc, char** argv, char** e) {
  char *name,*delimiter;
  char timestr[1024];
  DIR*    dir;
  struct dirent* dirent;
  time_t now;

  printf("Content-type: text/plain\n\n");

  printf("Arguments:\n");
  printf("argc => %d, argv => ",argc);
  while (argc>0) {
    printf("%s ",*argv++);
    argc--;
  }
  printf("\n\n");

  now = time(NULL);
  strftime(timestr,1024-1,"Localtime: %c %Z\n",localtime(&now));
  puts(timestr);

  printf("Identity:\n");
  printf("uid=%ld, gid=%ld\n\n",(long)getuid(),(long)getgid());

  printf("Current Directory: %s\n\n",getcwd(wd,MAXPATH));

  printf("Environment:\n");

  while ((name = *e++)) {
    delimiter = (char*) strchr(name,'=');
    if (delimiter == 0) continue;
    if (delimiter - name >= MAXENV) continue;
    strncpy(nm,name,delimiter-name);
    nm[delimiter-name]='\0';
    printf("%s => %s\n",nm,++delimiter);
  }
  return 0;
}
