To use sbox-dtc, we advise the below example Apache vhost configuration. Note
that /var/www/example.com should contain a full usable chroot disk, for example
built using debootstrap, or an image of your favorite Unix system.

With such a setup, both PHP, Python, Perl and Ruby scripts should work, served
by the SBOX chroot.

NameVirtualHost 10.0.0.1:80
<VirtualHost 10.0.0.1:80>
	ServerName www.example.com
	ScriptAlias /cgi-bin /usr/lib/cgi-bin
	AddHandler php-cgi-wrapper .php
	Action php-cgi-wrapper /cgi-bin/sbox
	AddHandler python-cgi-wrapper .py
	Action python-cgi-wrapper /cgi-bin/sbox
	AddHandler ruby-cgi-wrapper .rb
	Action ruby-cgi-wrapper /cgi-bin/sbox
	AddHandler ruby-cgi-wrapper .pl
	Action ruby-cgi-wrapper /cgi-bin/sbox

	Options +ExecCGI
	DocumentRoot /var/www/example.com/html
	RewriteEngine on
	RewriteCond %{REQUEST_URI} ^!/cgi-bin.*
	RewriteRule ^(.*) /cgi-bin/sbox/$1 [PT]
	DirectoryIndex index.php index.cgi index.pl index.py index.rb index.htm index.html index.php4
</VirtualHost>

Then, you would have things like this in your disk:
/var/www/example.com/{html,var,usr,tmp,...}

On another setup, you can as well do:

	DocumentRoot /var/www/example.com/var/www

This way, you can simply do:

	$ mkdir -p /var/www/example.com
	$ debootstrap --verbose --include=php5-cgi,wordpress \
		--arch amd64 /var/www/example.com http://ftp.us.debian.org

and then everything should follow, setup automatically, and work nicely, which
is a quite cool setup! :)

Note that now, what's annoying is apps that are hardly depending on a web
server to be installed (eg: you wont want one in the chroot).

Now, to start customizing SBOX for www.example.com, just do:

cp /etc/sbox/sbox.conf /etc/sbox/vhosts.d/www.example.com

and sbox will use this new file when it sees that HTTP_HOST value is for this
vhost. This way, it is trivial to have different configurations for different
sites on the same server.

Also, if you intend to run PHP as cgi-bin in the chroot, there are couples of
changes you have to make in your php.ini. Here's the directives:

doc_root = /html
cgi.force_redirect = 0

If you don't do that, PHP will complain about not having input.

Also, I would like to add here that if you want unbreakable chroot, it is
recommended to use the GR-Security kernels. Also, it is still possible to
destroy any kind of setup within the chroot, which is still annoying. For
example, your installed script would still be vulnerable to XSS scripting,
MySQL insertions and so on. I would recommend to use mod_security at the
same time as SBOX for these issues, so that you can have a chance to catch
issues before they even reach your applicaiton.

Enjoy the added security!

--
Thomas Goirand <zigo@debian.org>  Fri, 24 Jun 2011 20:32:12 +0800
