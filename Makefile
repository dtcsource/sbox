# Makefile for sbox.
# 1. Change the definition of CGI_DIRECTORY to point to your site-wide
#    cgi-bin directory
# 2. Review the INSTALL_OPTS definitions if you need to (will install
#    sbox SUID to root)
# 3. Change CFLAGS and CC definitions if you need to.
# 4. "make"
# 5. "make install"
VERSION=1.11

CC=gcc

COPTS= -O -g
#COPTS = -g -DDEBUG
#COPTS = -W -Wall -ansi -pedantic

STATIC_FLAG=-static

INSTALL_OPTS?= -o root -g bin -m 4111 -s
INSTALL_TARGET_DIR?=/usr/lib/cgi-bin
SYSCONFIG_DIR?=/etc
INSTALL_MY_SYSCONF_DIR=$(DESTDIR)$(SYSCONFIG_DIR)/sbox
INSTALL_DIRECTORY=$(DESTDIR)$(INSTALL_TARGET_DIR)

CFLAGS += $(COPTS) -DVERSION=$(VERSION)
INSTALL = install
LYNX    = lynx
PURIFY  = purify -chain-length="10" cc -g 

LIBS = -ldotconf

default: sbox

all: sbox env

sbox: sbox.o
	$(CC) -Wall $(LDFLAGS) $(LDOPTS) -o sbox sbox.o $(LIBS)
#	strip sbox
#	strip -R .comment -R .note sbox
	chmod 4711 sbox

sbox.o: sbox.c sbox.h Makefile

sbox_pure: sbox.o
	$(PURIFY) -o $@ sbox.o

env: env.o
	$(CC) $(STATIC_FLAG) -o env env.o

README.txt: README.html
	$(LYNX) -nolist -underscore -dump $< > $@

clean:
	rm -rf *.o core sbox env *~ CVS_sbox* *.tar.gz  build debian/tmp debian/substvars debian/files configure-stamp build-stamp
#        @-echo "OK, clean :)"

install: sbox
	mkdir -p $(INSTALL_DIRECTORY)
	$(INSTALL) $(INSTALL_OPTS) sbox $(INSTALL_DIRECTORY)
	$(INSTALL) -D -m 0644 sbox.conf $(INSTALL_MY_SYSCONF_DIR)/sbox.conf

dist:	README.txt sbox.h sbox.c env.c Makefile
	-mkdir sbox-$(VERSION)
	cp README.html README.txt sbox.h sbox.c env.c Makefile sbox-$(VERSION)
	tar cvf - sbox-$(VERSION) | gzip -9c > sbox-$(VERSION).tgz
	rm -rf sbox-$(VERSION)

deb:
	if [ -z $(SIGN)"" ] ; then \
		./deb ; \
	else \
		./deb --sign ; \
	fi

.PHONY: clean install rpm dist deb
