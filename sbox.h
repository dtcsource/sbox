/*
 * sbox.h -- definitions for the script box
 * Copyright 1997-1998, Lincoln D. Stein
 * $Revision: 1.5 $
 */

#ifndef _SBOX_H
#define _SBOX_H
#define TRUE  1
#define FALSE 0

/******        USER CONFIGURABLE DEFINES      ******/

/************** AbsolutePath ***************/
/* Added by Grant Kaufmann (grant@netizen.co.za) 28 March 2000
 *  In a case where you want to have sbox work on a directory
 *  not related to the webroot, you can define USE_ABSOLUTE_ROOT
 *  as that directory.
 * NOTE: the sbox binary you compile will work for that directory ONLY.
 * If you want to use it for another directory, recompile and use a
 * different binary
 */

#ifndef USE_ABSOLUTE_ROOT
/* #define USE_ABSOLUTE_ROOT "/home/myapp/html" */
#endif

/* This is a canned error statement */
#ifndef CANNED_ERROR_TOP
#define CANNED_ERROR_TOP \
"<HTML><HEAD><TITLE>Sbox Error</TITLE></HEAD> <BODY>" \
"<H1>Sbox Error</H1>" \
"The <A HREF=\"http://stein.cshl.org/WWW/software/sbox/\">sbox</A> " \
"program encountered an error while processing this request. " \
"Please note the time of the error, anything you might have been " \
"doing at the time to trigger the problem, and forward the " \
"information to this site's Webmaster "
#endif

#ifndef CANNED_ERROR_BOTTOM
#define CANNED_ERROR_BOTTOM \
"<HR>" \
"</BODY></HTML>"
#endif

#define CANNED_ERROR CANNED_ERROR_TOP CANNED_ERROR_BOTTOM

#endif /* _SBOX_H */
