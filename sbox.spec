Summary: A CGI wrapper script for chrooted environment for hosting
Name: sbox
Version: 1.11
Release: 0.20090803
License: LGPL
Group: System Environment/Daemons
URL: http://git.gplhost.com/dtc.git

Source: %{name}-%{version}.tar.gz

BuildRoot:%{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: make dotconf-devel gcc
Requires: dotconf

%description
Sbox is a CGI wrapper script that allows Web site hosting
services to safely grant CGI authoring privileges to untrusted
clients. In addition to changing the process privileges of client
scripts to match their owners, it goes beyond other wrappers by
placing configurable ceilings on script resource usage, avoiding
unintentional (as well as intentional) denial of service attacks. It
also optionally allows the Webmaster to place client's CGI scripts in
a chroot'ed shell restricted to the author's home directories.

%prep
%setup -n sbox

%build
make
make env

%install
%{__rm} -rf %{buildroot}
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} INSTALL_TARGET_DIR=%{_libdir}/cgi-bin install INSTALL_OPTS="-s" INSTALL_SYSCONF_DIR=%{_sysconfdir}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(0644, root, root, 0755)
%attr(4111, root, bin) %{_libdir}/cgi-bin/sbox
%config(noreplace) %{_sysconfdir}/sbox.conf

%changelog
* Mon Aug 03 2009 Thomas Goirand (zigo) <thomas@goirand.fr> 1.11-0.20090803
- Now packaging the sbox.conf, patched the Makefile to use INSTALL_SYSCONF_DIR
- Not using a patch file anymore
- Build depends on gcc
- Now the git includes sbox.spec directly
- Reworked the summary and description
- Bumped version to match reality

* Tue Jan 08 2008 Manuel Amador (Rudd-O) <rudd-o@rudd-o.com> $version-2.gplhost
- Fixed x86_64 installation

* Fri Dec 21 2007 Manuel Amador (Rudd-O) <rudd-o@rudd-o.com> 0.0.1-1.gplhost
- First construction for CentOS 5
